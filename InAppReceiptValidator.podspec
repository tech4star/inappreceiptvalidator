Pod::Spec.new do |spec|
    spec.name         = "InAppReceiptValidator"
    spec.version      = "1.0.0"
    spec.summary      = "InAppReceiptValidator framework"

    spec.source       = { :git => "git@bitbucket.org:tech4star/InAppReceiptValidator.git" }
    spec.homepage     = "https://bitbucket.org/tech4star/InAppReceiptValidator.git"

    spec.license          = 'MIT'
    spec.author           = { "Nikita Konopelko" => "nik.sativa@gmail.com" }
    spec.social_media_url = "https://www.facebook.com/Nik.Sativa"

    spec.ios.deployment_target = "12.0"
    spec.swift_version = '5.0'

    spec.dependency 'GRKOpenSSLFramework'
    spec.frameworks = 'Foundation', 'UIKit'

    spec.resources = ['Source/**/*.{storyboard,xib,xcassets,json,imageset,png,strings,stringsdict}']
    spec.source_files  = 'Source/**/*.swift'
    spec.exclude_files = 'Source/**/Test/**/*.*'
end
